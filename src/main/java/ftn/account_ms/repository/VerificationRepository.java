package ftn.account_ms.repository;
import ftn.account_ms.entity.Verification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VerificationRepository extends JpaRepository<Verification, Long> {

	Boolean verify(Verification req);

}
