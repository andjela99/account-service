package ftn.account_ms.controller;

import ftn.account_ms.entity.Verification;
import ftn.account_ms.service.VerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin
public class VerificationController {
	
	@Autowired
	private VerificationService verificationService;
	
	@RequestMapping(value = "/verify", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
  	public ResponseEntity<?> verifyProfile(@RequestBody Verification req) {
  		
  		return new ResponseEntity<>(verificationService.verify(req), HttpStatus.OK);
  	}

}
