package ftn.account_ms.service;


import ftn.account_ms.entity.Verification;

public interface VerificationService {

	Boolean verify(Verification req);

}
