package ftn.account_ms.service.impl;

import ftn.account_ms.repository.VerificationRepository;
import ftn.account_ms.entity.Verification;
import ftn.account_ms.service.VerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class VerificationServiceImpl implements VerificationService{
	
	@Autowired
	private VerificationRepository verificationRepository;

	@Override
	public Boolean verify(Verification req) {
		return verificationRepository.verify(req);
	}

}
