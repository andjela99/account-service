package ftn.account_ms.service.impl;

import ftn.account_ms.entity.User;
import ftn.account_ms.repository.UserRepository;
import ftn.account_ms.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public User findUser(String username, String password) {
		return userRepository.findByUsernameAndPassword(username,password);
	}

	@Override
	public User addUser(User user) {
		return userRepository.save(user);
	}

}
