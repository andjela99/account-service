package ftn.account_ms.service;


import ftn.account_ms.entity.User;

public interface UserService {

	User findUser(String username, String password);

	User addUser(User user);

}
